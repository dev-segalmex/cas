<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <title>SEGALMEX - Login</title>

    <!-- Bootstrap core CSS -->
    <link href="/util/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/util/css/all.min.css" rel="stylesheet" />

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      html,
      body {
        height: 100%;
      }

      body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
      }

      .form-signin {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: auto;
      }
      .form-signin .checkbox {
        font-weight: 400;
      }
      .form-signin .form-control {
        position: relative;
        box-sizing: border-box;
        height: auto;
        padding: 10px;
        font-size: 16px;
      }
      .form-signin .form-control:focus {
        z-index: 2;
      }
      .form-signin input[type="email"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
      }
      .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
      }

    </style>
  </head>
  <body class="text-center">

    <c:if test="${not pageContext.request.secure}">
      <div id="msg" class="errors">
        <h2><spring:message code="screen.nonsecure.title" /></h2>
        <p><spring:message code="screen.nonsecure.message" /></p>
      </div>
    </c:if>

    <div id="cookiesDisabled" class="errors" style="display:none;">
      <h2><spring:message code="screen.cookies.disabled.title" /></h2>
      <p><spring:message code="screen.cookies.disabled.message" /></p>
    </div>


    <c:if test="${not empty registeredService}">
      <c:set var="registeredServiceLogo" value="images/webapp.png"/>
      <c:set var="registeredServiceName" value="${registeredService.name}"/>
      <c:set var="registeredServiceDescription" value="${registeredService.description}"/>

      <c:choose>
        <c:when test="${not empty mduiContext}">
          <c:if test="${not empty mduiContext.logoUrl}">
            <c:set var="registeredServiceLogo" value="${mduiContext.logoUrl}"/>
          </c:if>
          <c:set var="registeredServiceName" value="${mduiContext.displayName}"/>
          <c:set var="registeredServiceDescription" value="${mduiContext.description}"/>
        </c:when>
        <c:when test="${not empty registeredService.logo}">
          <c:set var="registeredServiceLogo" value="${registeredService.logo}"/>
        </c:when>
      </c:choose>

      <div id="serviceui" class="serviceinfo" style="display: none">
        <table>
          <tr>
            <td><img src="${registeredServiceLogo}"></td>
            <td id="servicedesc">
              <h1>${fn:escapeXml(registeredServiceName)}</h1>
              <p>${fn:escapeXml(registeredServiceDescription)}</p>
            </td>
          </tr>
        </table>
      </div>
      <p/>
    </c:if>

    <form:form class="form-signin" method="post" id="fm1" commandName="${commandName}" htmlEscape="true" autocomplete="off">
      <p class="text-center"><img class="mb-4" src="/util/images/segalmex.jpg" alt="segalmex" style="width:100%"></p>
      <form:errors path="*" id="msg" cssClass="alert alert-warning" element="div" htmlEscape="false" />
      <label for="username" class="sr-only">Correo electrónico</label>
      <input id="username" name="username" type="text" class="form-control" placeholder="Correo electrónico" required autofocus>
      <label for="password" class="sr-only">Contraseña</label>
      <input id="password" name="password" type="password" class="form-control" placeholder="Contraseña" required>
      <input type="hidden" name="execution" value="${flowExecutionKey}" />
      <input type="hidden" name="_eventId" value="submit" />	
      <button type="submit" id="submit-forma" style="display: none;"></button>
      <button id="entrar" name="submit" accesskey="l" class="btn btn-lg btn-primary btn-block" type="button">Entrar</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
    </form:form>

    <script src="/util/js/dist/jquery-3.4.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function () {
          $("#entrar").click(function (event) {
              event.preventDefault();
              if ($("#username").val() === "") {
                  alert("Verifique los siguientes campos", "<ul><li><strong>Usuario</strong> es requerido.</li></ul>");
                  return false;
              }
              if ($("#password").val() === "") {
                  alert("Verifique los siguientes campos", "<ul><li><strong>Constraseña</strong> es requerida.</li></ul>");
                  return false;
              }
              $("#submit-forma").click();
          });
      });
    </script>
  </body>
</html>