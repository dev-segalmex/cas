<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SEGALMEX - Logout</title>

    <!-- Bootstrap core CSS -->
    <link href="/util/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/util/css/all.min.css" rel="stylesheet" />
    <link href="/util/css/segalmex.css" rel="stylesheet"/>
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">
          <img height="32" src="/util/images/segalmex-brand.png" alt="Segalmex"> Segalmex
        </a>
      </div>
    </nav>
    <main role="main" class="container">      
      <div class="container" id="bd-ct" style="min-height: 400px;">      
        <div class="page-header">
          <h1>Ha salido completamente del sistema</h1>
        </div>        
        <p>Si desea ingresar nuevamente al sistema de clic <a href="/pluss/principal.html">aquí</a></p>
      </div>
    </main>
    <footer id="main-footer">
      <div class="container"><p>&nbsp;</p><p>&nbsp;</p></div>
      <div class="container-fluid footer-pleca"></div>
    </footer>
    <div>
      <script src="/util/js/dist/jquery-3.4.1.min.js" type="text/javascript"></script>
      <script src="/util/js/dist/bootstrap.bundle.min.js" type="text/javascript"></script>
    </div>
  </body>
</html>